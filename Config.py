#!/usr/local/bin/python3
# _*_ coding:utf-8 _*_

# 保存项目配置

# 通知开关
NOTIFY_OPTION = {
    "dingding": True,
    "server_chan": True,
}

# redis连接信息，用于保存已经通知过的消息
REDIS = {
    "host":"127.0.0.1",
    "port": 6379,
    "index": 1,
    "key": "read",  # 使用set保存，sadd read message_id
}


# 钉钉通知 help: https://ding-doc.dingtalk.com/doc#/serverapi2/isu6nk
DINGDING = [
    {
      "header": {"content-type":"application/json"},
      "url": "",
      "template": {"msgtype": "text", "text": {"content": "我的头发长，天下我为王"}},
    }
]

# Server酱(微信)通知,想通知多个人，按照下面的格式添加到列表中即可 help: http://sc.ftqq.com/3.version
SERVER_CHAN = [
    {
      "token": "",  # 你的token
      "is_filter": True,
      "keywords": ["甘肃","广东"],
    }
]
