#!/usr/local/bin/python3
# _*_ coding: utf-8 _*_

from util import ColorUtil,RedisConn
from notify import DingDing,ServerChan
import Config

item = {
    "id": 1000000000,
    "title": "测试消息标题",
    "summary": "测试消息内容",
    "infoSource": "测试消息来源"
}


def test_redis():
    redis_conn = RedisConn()
    redis_conn.pingpingping()

def test_dingding():
    if not Config.NOTIFY_OPTION["dingding"]:
        return
    ding = DingDing()
    if ding.send(item):
        print("{}钉钉消息发送成功{}".format(ColorUtil.get_green(),ColorUtil.get_reset()))
    else:
        print("{}钉钉消息发送失败{}".format(ColorUtil.get_red(),ColorUtil.get_reset()))

def test_serverChan():
    if not Config.NOTIFY_OPTION["server_chan"]:
        return
    s = ServerChan()
    if s.send(item):
        print("{}Server酱消息发送成功{}".format(ColorUtil.get_green(),ColorUtil.get_reset()))
    else:
        print("{}Server酱消息发送失败{}".format(ColorUtil.get_red(),ColorUtil.get_reset()))

def test_all():
    test_redis()
    test_dingding()
    test_serverChan()
