#!/usr/local/bin/python3
# _*_ coding:utf-8 _*_

import Config
import redis

class ColorUtil():
    @staticmethod
    def get_reset():
        return "\033[0m"

    @staticmethod
    def get_red_with_back():
        return "\033[41;37;1m"
    @staticmethod
    def get_red():
        return "\033[31m"

    @staticmethod
    def get_green_with_back():
        return "\033[97;42m"
    @staticmethod
    def get_green():
        return "\033[32m"

    @staticmethod
    def get_blue_with_back():
        return "\033[97;44m"

    @staticmethod
    def get_blue():
        return "\033[34m"

class RedisConn():
    def __init__(self):
        self.host = Config.REDIS["host"]
        self.port = Config.REDIS["port"]
        self.db = Config.REDIS["index"]
        self.set_key = Config.REDIS["key"]
        self.conn = redis.Redis(host=self.host,port=self.port,db=self.db)

    def add(self,id):
        return self.conn.sadd(self.set_key,id)

    def check_is_in(self,id):
        res = self.conn.sismember(self.set_key,id)
        return True if res is True else False

    def pingpingping(self):
        if self.conn.ping():
            print("{}redis连接正常{}".format(ColorUtil.get_green(),ColorUtil.get_reset()))
        else:
            print("{}redis连接失败{}".format(ColorUtil.get_red(),ColorUtil.get_reset()))
