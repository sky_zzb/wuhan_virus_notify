#!/usr/local/bin/python3
# _*_ coding:utf-8 _*_

import argparse
import sys

help = "r: 运行程序, t: 检查各组件是否正常工作"

if __name__ == "__main__":
    parser = argparse.ArgumentParser("python3 run.py")
    parser.add_argument("op",type=str,help=help)
    op = parser.parse_args(sys.argv[1:])
    if op.op == "r":
        from internal import DingXiang
        dingxiang = DingXiang()
        dingxiang.run()
    elif op.op == "t":
        from test import test_all
        test_all()
    else:
        print("参数错误")
        print("用法： python3 run.py " + help)

